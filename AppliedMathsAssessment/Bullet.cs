﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using System;

namespace AppliedMathsAssessment
{
    class Bullet : ModelObject
    {
        // ------------------
        // Data
        // ------------------
        private Game1 game;
        private SoundEffect cannonHit;
        private SoundEffect enemyExplode;


        // ------------------
        // Behaviour
        // ------------------
        public void Initialise(Game1 newGame, SoundEffect newCannonHit, SoundEffect newEnemyExplode)
        {
            drag = 0f;
            scale = new Vector3(0.00001f);
            useGravity = true;
            gravityScale = 10f;
            game = newGame;
            cannonHit = newCannonHit;
            enemyExplode = newEnemyExplode;
        }
        // ------------------
        public void CopyTo(Bullet newObject)
        {
            CopyTo(newObject as ModelObject);
            newObject.Initialise(game, cannonHit, enemyExplode);
        }
        // ------------------
        public void Fire(Vector3 startingPosition, Vector3 newVelocity)
        {
            position = startingPosition;
            velocity = newVelocity;
            Vector3 direction = velocity;
            direction.Normalize();
            rotation.X = MathHelper.ToRadians(90f) - (float)Math.Asin(direction.Y);
            rotation.Y = (float)Math.Atan2(direction.X, direction.Z);
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            // Update facing based on movement
            Vector3 direction = velocity;
            direction.Normalize();

            rotation.X = MathHelper.ToRadians(90f) - (float)Math.Asin(direction.Y);
            rotation.Y = (float)Math.Atan2(direction.X, direction.Z);

            base.Update(gameTime);
        }
        // ------------------
        public override void HandleCollision(PhysicsObject other)
        {
            if (other is Enemy)
            {
                game.RemoveObject(other);
                enemyExplode.Play();
                game.RemoveObject(this);
            }
            else if (other is BasicCuboid) // wall
            {
                cannonHit.Play();
                game.RemoveObject(this);
            }
        }
        // ------------------
    }
}
